from flask import Flask
from flask import render_template
from flask import request
from flask import Response
from werkzeug.utils import secure_filename
import re

app = Flask(__name__)
#app.debug=True

count=3

@app.route('/')
def index():
    return render_template('index.html')

@app.route('/output.txt', methods=['POST', 'GET'])
def concat():
    f=None
    try:
        f=request.files['srcfile']
    except KeyError:
        return 'File non specificato! =('
    print dir(f)
    s=''
    for i,l in enumerate(f):
        s+=re.sub('[\r\n]','',l)+(' ' if (i+1)%3 else '\n')
    r=Response(s, mimetype='text/plain')
    r.headers['Content-Disposition'] = 'attachment; filename=out_' + secure_filename(f.filename)
    return r


if __name__=='__main__':
    app.run()
